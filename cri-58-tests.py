"""Testing playground for the latest evolution of the CBOR form for CRIs"""
# from cri-complete.cddl
#
# CRI-Reference = [
#   ?( ((scheme, (authority / null) 
#        // (null, authority))
#      // discard),                 ; relative reference
#      ?( (path / null, query / null, fragment) //
#         (path / null, query) //
#          path)
#   )
# ]
# 
# scheme      = scheme-name / scheme-id
# scheme-name = text .regexp "[a-z][a-z0-9+.-]*"
# scheme-id   = (COAP / COAPS / HTTP / HTTPS / other-scheme)
#               .within nint
# COAP = -1 COAPS = -2 HTTP = -3 HTTPS = -4
# other-scheme = nint .feature "scheme-id-extension"
# 
# authority   = host / [host, port]
# host        = host-name / host-ip
# host-name   = text
# host-ip     = bytes .size 4 / bytes .size 16
# port        = 0..65535
# 
# discard     = true / 0..127
# path        = [*text]
# query       = [*text]
# fragment    = text

from __future__ import annotations

from typing import Optional, Union, Tuple
from dataclasses import dataclass
import io
import sys

import cbor2

class PeakableCborStream:
    """A CBOR stream that supports type peeking, ie. making decisions on the
    coming-up type without consuming that type. Peaking is conflated with type
    information to avoid constructing the actual parsed type twice, and for
    CBOR-level access to the type information. (eg. unsigned integer
    and negative integer, where booleans don't count as integers).

    >>> stream = cbor2.dumps(['a', b'a', 4, 1])[1:]
    >>> stream = PeakableCborStream(stream)
    >>> stream.peek_class()
    3
    >>> next(stream)
    'a'
    >>> stream.peek_class()
    2
    >>> next(stream)
    b'a'
    >>> stream.peek_is_uint()
    True
    """
    def __init__(self, encoded: bytes):
        self.__tail = io.BytesIO(encoded)

    def __next__(self):
        if self.__tail.tell() == len(self.__tail.getbuffer()):
            raise StopIteration
        return cbor2.load(self.__tail)

    def _peek_byte(self):
        return self.__tail.getbuffer()[self.__tail.tell()]

    def peek_major(self):
        return self._peek_byte() >> 5

    def peek_is_uint(self):
        return self.peek_major() == 0

    def peek_is_nint(self):
        return self.peek_major() == 1

    def peek_is_bstr(self):
        return self.peek_major() == 2

    def peek_is_tstr(self):
        return self.peek_major() == 3

    def peek_is_false(self):
        return self._peek_byte() == 0xf4

    def peek_is_true(self):
        return self._peek_byte() == 0xf5

@dataclass(frozen=True)
class CRIref:
    # invariant hard to express in Python: if scheme or authority is set, this needs to be True
    discard: Union[int, True]
    scheme: Optional[Union[str, int]]
    # Slightly different than the host-or-array native version, but easier to
    # access from Python
    authority: Optional[Tuple[Union[bytes, str], Optional[int]]]
    path: Optional[list[str]]
    query: Optional[list[str]]
    fragment: Optional[str]

    @classmethod
    def from_bytes(cls, href: bytes):
        """Find the parts from a Python list created from creating a CBOR
        stream CRI reference as elements of an array

        In a constrained implementation (like in Rust using serde-cbor), the
        dissection would happen from driving the CBOR implementation rather
        than from constructing a variadic object and then splitting this up.
        """
        # Expressed in iteration because that's similar to how an embedded
        # processor might do it (but honestly this is probably best written by
        # a CDDL processor)
        #
        # When in an embedded system there is no preprocessing, this can be
        # equivalently expressed as an iterator over semantic components of the
        # unpreprocessed CRI reference.

        parsed = PeakableCborStream(href)

        discard = True
        scheme = None
        authority = None
        path = None
        query = None
        fragment = None

        try:
            if parsed.peek_is_uint() or parsed.peek_is_true():
                discard = next(parsed)
            else:
                scheme = next(parsed)
                assert type(scheme) in (str, int, type(None))

                authority = next(parsed)
                if authority is not None:
                    if not isinstance(authority, list):
                        authority = [authority, None]

                    assert type(authority[0]) in (bytes, str)
                    assert type(authority[1]) in (int, type(None))

            path = next(parsed)
            if path:
                assert all(isinstance(p, str) for p in path)

            query = next(parsed)
            if query:
                assert all(isinstance(q, str) for q in query)

            fragment = next(parsed)
            assert isinstance(fragment, str)
        except (StopIteration, IndexError):
            # Care needs to be taken at every `next` and `peek_*` that all
            # pre-configured locals are filled before raising
            pass

        # see also https://github.com/core-wg/coral/pull/76#issuecomment-868689593
        if path is None and discard != 0:
            print("Path should have been empty here. CDDL can't know while generating, fixing")
            path = []

        # FIXME: If we were using a nice CBOR parser, or the CBOR parser
        # nicely, we could now report that self-delimitation terminated and
        # that some bytes may be left over.

        return CRIref(discard, scheme, authority, path, query, fragment)

    def as_rough_uri_ref(self) -> str:
        """Crude visualization with no ambitions of correctness, especially
        w/rt escaping and IP address representation"""
        result = ""

        if self.scheme is None and self.authority is None and self.discard == 0 and self.path:
            result += "(can't be expressed, we're appending path segments)"

        if self.scheme is not None:
            schemes = {-1: "coap", -2: "coaps", -3: "http", -4: "https"}
            # FIXME: Complain on unknown schemes or just do a sloppy conversion?
            result = schemes.get(self.scheme, str(self.scheme)) + ":"

        if self.authority:
            host, port = self.authority
            if isinstance(host, bytes):
                host = "[" + host.hex() + "]" # not precisely but close enough
            result += "//" + host
            if port is not None:
                result += ":%d" % port
            result += "/"

        if self.discard is True:
            if (self.scheme, self.authority) == (None, None):
                result += "/"
        elif self.discard == 0:
            pass
        elif self.discard == 1:
            result += "./" # not strictly necessary in all cases, but better safe than sorry when a ':' comes along later
        elif self.discard < 5:
            result += "../" * (self.discard - 1)
        else:
            result += "(%d*)../ " % (self.discard - 1)

        if self.path:
            result += self.path[0]
            for p in self.path[1:]:
                result += "/" + p
        if self.query:
            result += "?"
            result += self.query[0]
            for q in self.query[1:]:
                result += "&" + q

        if self.fragment:
            result += "#" + self.fragment

        return result

    def is_uri(self):
        """Return True if the reference is in full URI form

        This is commonly (but subtly wrongly) referred to as "absolute URI".
        """
        return self.scheme is not None

    def resolve_against(self, base: CRIref) -> CRIref:
        if not base.is_uri():
            raise ValueError("Relative resolution is only well-defined for "
                    "CRIref bases with a scheme")

        if self.scheme is not None:
            return self

        if self.authority is not None:
            return CRIref(True, base.scheme,
                    # *self[2:]
                    self.authority,
                    self.path,
                    self.query,
                    self.fragment,
                    )

        if self.discard == 0:
            path = base.path + (self.path or [])
        elif self.discard is True:
            path = self.path
        else:
            path = base.path[:-self.discard] + self.path

        return CRIref(
                True,
                base.scheme,
                base.authority,
                path,
                self.query if (self.path is not None or self.query is not None) else base.query,
                self.fragment
                )

def generate_from_cddl(count):
    import re
    import subprocess
    output = subprocess.check_output('~/.gem/ruby/2.7.0/bin/cddl /home/chrysn/git/ietf-drafts/coral/cddl/cri-complete.cddl generate %d' % count, shell=True)

    for line in output.split(b'\n'):
        if not line:
            # empty last line
            continue
        actual_cbor = subprocess.run("~/.gem/ruby/2.7.0/bin/diag2cbor.rb", shell=True, input=line, capture_output=True).stdout
        yield cbor2.loads(actual_cbor)

if __name__ == "__main__":
    canonical_base = CRIref(True, -3, ("a", None), ["b", "c", "d;p"], ["q"], None) # http://a/b/c/d;p?q

    items = list(generate_from_cddl(20)) + [
            # as_rough_uri_ref doesn't give the slash and ampersand the treatment they should, which is why it's rough
            ["HtTpS", "example.com", ["some", "pa/path"], ["query", "query&query"], "fragment"],
            [None, bytes.fromhex("7f000001")],
            ["mailto", None, ["user@example.com"], ["subject=foo"]],
            [None, ["localhost", 8080]],
            [],
            [0, ["append"]],
            [0, None, []],
            [0, None, None, "fragment"],
            [0, None, [], "fragment"],
            ]

    for liststyle in items:
        print(liststyle)
        encoded = b"".join(cbor2.dumps(x) for x in liststyle)
        parsed = CRIref.from_bytes(encoded)
        print(parsed)
        print(parsed.as_rough_uri_ref())

        resolved = parsed.resolve_against(canonical_base)
        print(resolved)
        print(resolved.as_rough_uri_ref())
        print()
