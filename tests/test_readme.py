import unittest
import subprocess
import functools
import itertools
import sys

commandstart = '    $ '

class TestREADME(unittest.TestCase):
    def test_readme_commands(self):
        with open('README.rst') as readme:
            commands_and_tails = functools.reduce(lambda old, l: [*old, [l]] if l.startswith(commandstart) else (old[-1].append(l), old)[1], readme, [[]])
            for block in commands_and_tails:
                if not block[0].startswith(commandstart + './micrurus-util'):
                    # first block or non-micrurus command
                    continue
                command = block.pop(0)[len(commandstart):].strip()
                expected_output = "".join(x[4:] for x in itertools.takewhile(lambda x: x.startswith('    '), block))

                command_output = subprocess.check_output(command, shell=True).decode('utf8')
                if expected_output:
                    self.assertEqual(expected_output, command_output, "Output differences for %s" % (command,))
