import unittest
import functools
import re

from micrurus.iri import IRI

class Test3986Examples(unittest.TestCase):
    """RFC 3986 examples

    From Section 1.1.2 -- should just be expressible round-trippable through
    IRIs.
    """
    examples = """ftp://ftp.is.co.za/rfc/rfc1808.txt

      http://www.ietf.org/rfc/rfc2396.txt

      ldap://[2001:db8::7]/c=GB?objectClass?one

      mailto:John.Doe@example.com

      news:comp.infosystems.www.servers.unix

      tel:+1-816-555-1212

      telnet://192.0.2.16:80/

      urn:oasis:names:specification:docbook:dtd:xml:4.1.2
      """

    def test_examples(self):
        lines = self.examples.split("\n")
        lines = (l.strip() for l in lines)
        lines = (l for l in lines if l)
        for ex in lines:
            as_iri = IRI.decompose(ex)
            recomposed = as_iri.recompose()

            self.assertEqual(ex, recomposed, "Recomposition failed, intermediate was %r" % (as_iri,))

class Test3986Resolution(unittest.TestCase):
    base = IRI.decompose("http://a/b/c/d;p?q")

    normal = """
      "g:h"           =  "g:h"
      "g"             =  "http://a/b/c/g"
      "./g"           =  "http://a/b/c/g"
      "g/"            =  "http://a/b/c/g/"
      "/g"            =  "http://a/g"
      # Modified: The normalized form has a trailing slash, and CoRAL IRIs are
      # produced normalized.
      "//g"           =  "http://g/"              # was: "http://g"
      "?y"            =  "http://a/b/c/d;p?y"
      "g?y"           =  "http://a/b/c/g?y"
      "#s"            =  "http://a/b/c/d;p?q#s"
      "g#s"           =  "http://a/b/c/g#s"
      "g?y#s"         =  "http://a/b/c/g?y#s"
      ";x"            =  "http://a/b/c/;x"
      "g;x"           =  "http://a/b/c/g;x"
      "g;x?y#s"       =  "http://a/b/c/g;x?y#s"
      ""              =  "http://a/b/c/d;p?q"
      # Disabled pending discussion of explicit dot and dot-dot segments (alternatively, we could encode any rel-ref that ends in a "." or ".." group with an empty component in the binary form, or rather adapt the algorithm)
      "."             !=  "http://a/b/c/"
      "./"            =  "http://a/b/c/"
      # Disabled pending discussion of explicit dot and dot-dot segments
      ".."            !=  "http://a/b/"
      "../"           =  "http://a/b/"
      "../g"          =  "http://a/b/g"
      "../.."         =  "http://a/"
      "../../"        =  "http://a/"
      "../../g"       =  "http://a/g"
    """

    abnormal = """
      "../../../g"    =  "http://a/g"
      "../../../../g" =  "http://a/g"

      "/./g"          =  "http://a/g"
      "/../g"         =  "http://a/g"
      "g."            =  "http://a/b/c/g."
      ".g"            =  "http://a/b/c/.g"
      "g.."           =  "http://a/b/c/g.."
      "..g"           =  "http://a/b/c/..g"

      "./../g"        =  "http://a/b/g"
      "./g/."         =  "http://a/b/c/g/"
      "g/./h"         =  "http://a/b/c/g/h"
      "g/../h"        =  "http://a/b/c/h"
      "g;x=1/./y"     =  "http://a/b/c/g;x=1/y"
      "g;x=1/../y"    =  "http://a/b/c/y"

      "g?y/./x"       =  "http://a/b/c/g?y/./x"
      "g?y/../x"      =  "http://a/b/c/g?y/../x"
      "g#s/./x"       =  "http://a/b/c/g#s/./x"
      "g#s/../x"      =  "http://a/b/c/g#s/../x"

      "http:g"        =  "http:g"
      """

    @staticmethod
    def _split_list(examples):
        return re.findall(r'"([^"]*)"\s*=\s*"([^"]*)"', examples)

    def test_normal(self):
        for reference, expectation in self._split_list(self.normal):
            ref_iri = IRI.decompose(reference)
            resolved = self.base / ref_iri
            recomposed = resolved.recompose()
            self.assertEqual(expectation, recomposed, "Resolution failed, reference was %r (%r, should be %r)" % (ref_iri, list(ref_iri), reference))
