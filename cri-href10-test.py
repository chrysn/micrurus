from typing import Optional, Union, List, Tuple, NewType, Set
import ipaddress
import string
import copy

import cbor2

class NotExpressibleAsUri(ValueError):
    """The CRI reference whose conversion to a URI reference is attempted can
    not be expressed as a URI reference."""

class Singleton:
    __instance = {}

    def __new__(cls):
        try:
            return cls.__instance[cls]
        except KeyError:
            s = super().__new__(cls)
            cls.__instance[cls] = s
            return s

    def __repr__(self):
        return "%s()" % type(self).__name__

class NoauthNoslash(Singleton):
    serialized = True

class NoauthLeadingslash(Singleton):
    serialized = None

class DiscardAll(Singleton):
    serialized = True

unreserved = set(string.ascii_letters + string.digits + '-._~')
subdelims = set("!$&'()*+,;=")
gendelims = set(":/?#[]@")
reserved = gendelims.union(subdelims)

class TextOrPet:
    _inner: Union[str, list]

    # Characters that are just copied over from a str to the URI component (in addition to unreserved)
    # Consequently, any of those may be present in bytes components (in
    # addition to high characters that are neither reserved nor unrserved)
    unescaped_reserved: Set[str]

    def __init__(self, inner):
        self._inner = inner

    def __repr__(self):
        return "%s(%r)" % (type(self).__name__, self._inner)

    @classmethod
    def from_serialized_unsafe(cls, cboritem: Union[str, list]) -> "Self": # Python 3.11: Self
        assert type(cboritem) in (str, list)
        return cls(cboritem)

    # How much checking would a safe from_serialized do? Seeing whether
    # somewhere in the hex there is a valid UTF-8 sequence would be rather
    # expensive, and I'm not sure how relevant it is. (cf.
    # https://github.com/core-wg/href/issues/44)

    def to_uri_part(self) -> str:
        iterable = self._inner if isinstance(self._inner, list) else [self._inner]
        result = ""
        for item in iterable:
            if isinstance(item, str):
                for c in item:
                    if c in unreserved or c in self.unescaped_reserved:
                        result += c
                    else:
                        result += "".join("%%%02X" % x for x in c.encode('utf8'))
            else:
                assert isinstance(item, bytes)
                for c in item:
                    result += "%%%02X" % c
        return result

class TextOrPetForUserinfo(TextOrPet):
    unescaped_reserved = subdelims

class TextOrPetForHost(TextOrPet):
    # SPEC also says dot, but https://github.com/core-wg/href/issues/43
    unescaped_reserved = subdelims

    # FIXME add extra checks to see that "." is neither in bytes nor in str?

class TextOrPetForPath(TextOrPet):
    unescaped_reserved = subdelims.union({":", "@"})

class TextOrPetForQuery(TextOrPet):
    unescaped_reserved = subdelims.union({":", "@", "/", "?"}).difference({"&"})

class TextOrPetForFragment(TextOrPet):
    unescaped_reserved = subdelims.union({":", "@", "/", "?"}) # leaving "[", "]" and "#" as well as any neither-reserved-nor-unreserved

class TextOrPetList(list):
    # FIXME: How would I annotate that this list always contains cls.TextOrPet?
    TextOrPet: type

    def __repr__(self):
        # Reaching into the Inner because the newtype information on it is
        # needless when this type's name is already shown
        return "%s([%s])" % (type(self).__name__, ", ".join(repr(i._inner) for i in self))

    @classmethod
    def from_serialized_unsafe(cls, cboritem: list) -> "Self": # Python 3.11: Self
        return cls([cls.TextOrPet.from_serialized_unsafe(c) for c in cboritem])

    def to_uri_part(self):
        return self.delimiter.join(p.to_uri_part() for p in self)

class TextOrPetListHost(TextOrPetList):
    TextOrPet = TextOrPetForHost
    delimiter = "."

class TextOrPetListPath(TextOrPetList):
    TextOrPet = TextOrPetForPath
    delimiter = "/"

class TextOrPetListQuery(TextOrPetList):
    TextOrPet = TextOrPetForQuery
    delimiter = "&"

class Authority:
    userinfo: Optional[TextOrPetForUserinfo]
    host: Union[Tuple[bytes, str], TextOrPetListHost]
    port: Optional[int]

    def __repr__(self):
        return "<%s %s>" % (type(self).__name__, vars(self))

    def from_serialized_unsafe(cboritem) -> "Authority": # python 3.11: Self
        self = Authority()

        if cboritem[0] == False:
            cboritem.pop(0)
            self.userinfo = TextOrPetForUserinfo.from_serialized_unsafe(cboritem.pop(0))
        else:
            self.userinfo = None
        if isinstance(cboritem[0], bytes):
            ip = cboritem.pop(0)
            if cboritem and isinstance(cboritem[0], str):
                self.host = (ip, cboritem.pop(0))
            else:
                self.host = (ip, None)
        else:
            host = []
            while cboritem and type(cboritem[0]) in (str, list):
                popped = cboritem.pop(0)
                host.append(popped)
            # SPEC: this is the only time we build this not from an explicit list but from a group while it matches
            # https://github.com/core-wg/href/issues/50
            self.host = TextOrPetListHost.from_serialized_unsafe(host)

        if cboritem:
            self.port = cboritem.pop(0)
            assert isinstance(self.port, int)
        else:
            self.port = None

        assert not cboritem

        return self

    def to_uri_part(self) -> str:
        if isinstance(self.host, tuple):
            if len(self.host[0]) == 4:
                result = str(ipaddress.IPv4Address(self.host[0]))
                assert self.host[1] is None
            elif len(self.host[0]) == 16:
                result = str(ipaddress.IPv6Address(self.host[0]))
                if self.host[1] is not None:
                    # FIXME scaping?
                    result += '%25' + self.host[1]
            else:
                raise ValueError("Odd IP address length")
        else:
            result = self.host.to_uri_part()

        if self.userinfo is not None:
            result = self.userinfo.to_uri_part() + '@' + result

        if self.port is not None:
            result += ':' + str(self.port)

        return result

Scheme = NewType("Scheme", Optional[Union[int, str]])
Authorityish = NewType("Authorityish", Union[Authority, NoauthNoslash, NoauthLeadingslash])

def authorityish_from_serialized_unsafe(cboritem) -> Authorityish:
    if cboritem == NoauthNoslash.serialized:
        return NoauthNoslash()
    elif cboritem == NoauthLeadingslash.serialized:
        return NoauthLeadingslash()
    else:
        return Authority.from_serialized_unsafe(cboritem)

class CRIRef:
    # FIXME: Express that all but internals better treat this as immutable ... deeply?

    scheme: Optional[Scheme]
    # Not expressed in the type annotation: Needs to be set if scheme is set, 
    authority: Optional[Authorityish]
    discard: Union[int, DiscardAll]
    path: Optional[TextOrPetListPath]
    query: Optional[TextOrPetListQuery]
    Fragment: Optional[TextOrPetForFragment]

    def __repr__(self):
        return "<%s %s>" % (type(self).__name__, vars(self))

    @classmethod
    def from_serialized_unsafe(cls, cborlist: list) -> "CRIRef": # Self, starting from Python 3.11
        """Create a CRI from a list decoded through cbor2's default decoder.

        This is unsafe in that it uses asserts for validity constraints (and
        doesn't even claim to check all), and in that it makes no attempt to
        raise validity errors in any kind of consistent error type.
        """
        self = CRIRef()

        # one of the many branches in the initial condition will overwrite the,
        # the other will need them like this
        self.scheme = None
        self.authority = None
        self.discard = DiscardAll()

        # Note the 'is': We only want to catch True, not 1 to which it is equal
        if not cborlist or cborlist[0] is DiscardAll.serialized:
            self.discard = DiscardAll()
            if cborlist:
                cborlist.pop(0)
            else:
                # filling in tail-trimmed None, which means:
                self.authority = NoauthLeadingslash()
        elif isinstance(cborlist[0], int) and cborlist[0] >= 0:
            # start with discard
            self.discard = cborlist.pop(0)
        else:
            # regular scheme and authority
            self.scheme = cborlist.pop(0)
            assert self.scheme is None or isinstance(self.scheme, str) or (isinstance(self.scheme, int) and self.scheme < 0)

            self.authority = authorityish_from_serialized_unsafe(cborlist.pop(0) if cborlist else None)

        # FIXME: ergonomics would improve if we cborlist was wrapped in something which empty still produces a pop(0) of None
        path = cborlist.pop(0) if cborlist else None
        if self.discard != 0 and path is None:
            # SPEC: I need this for comparisons... https://github.com/core-wg/href/issues/50
            path = []
        self.path = TextOrPetListPath.from_serialized_unsafe(path) if path is not None else None
        query = cborlist.pop(0) if cborlist else None
        self.query = TextOrPetListQuery.from_serialized_unsafe(query) if query is not None else None
        fragment = cborlist.pop(0) if cborlist else None
        self.fragment = TextOrPetForFragment.from_serialized_unsafe(fragment) if fragment is not None else None

        return self

    def is_cri(self) -> bool:
        """Is the CRI reference a (full) CRI?"""
        return self.scheme is not None

    def resolve(self, base: "CRIRef") -> "CRIRef": # Python 3.11: Self
        if not base.is_cri():
            raise ValueError("Base needs to be a full CRI")

        if self.scheme is not None:
            # shortcut
            return copy.deepcopy(self)

        result = copy.deepcopy(base)

        if self.discard is DiscardAll():
            result.path[:] = []
        elif self.discard > 0:
            result.path[-self.discard:] = []
        if self.discard != 0 or self.path is not None:
            result.query = None
            result.fragment = None

        if self.authority is not None:
            result.authority = self.authority

        if self.path is not None:
            result.path.extend(self.path)

        if self.query is not None:
            result.query = self.query
            result.fragment = None

        if self.fragment is not None:
            result.fragment = self.fragment

        return result

    def __eq__(self, other):
        # FIXME
        return repr(self) == repr(other)

    def to_uri_ref(self) -> str:
        if self.scheme is not None:
            if isinstance(self.scheme, int):
                # SPEC https://github.com/core-wg/href/pull/49
                uri = self.known_schemes[self.scheme]
            else:
                uri = self.scheme
            uri += ':'
        else:
            uri = ''

        if isinstance(self.authority, Authority):
            uri += '//' + self.authority.to_uri_part()

        if self.authority is NoauthLeadingslash() and self.scheme is None:
            # FIXME didn't find it in SPEC
            raise NotExpressibleAsUri

        if self.discard == 0 and self.path is not None:
            raise ValueError("Can not express appending paths")
        if self.discard is not DiscardAll() and self.discard > 0:
            assert self.scheme is None, "Malformed internal state"
            assert self.authority is None, "Malformed internal state"
            uri += "../" * (self.discard - 1)
            rooted = False
        elif self.authority is NoauthNoslash():
            rooted = False
        else:
            rooted = True

        if self.path:
            # leading slash explicitly added because to_uri_part only adds inner delimiters
            path = "/" + self.path.to_uri_part()
        else:
            if self.discard is DiscardAll() and self.authority is None and self.scheme is None:
                # FIXME it's probably one of the below statements
                raise NotExpressibleAsUri
            path = ""
        if not rooted:
            path = path.removeprefix('/')
        if ':' in path and ('/' not in path or path.index(':') < path.index('/')):
            # SPEC https://github.com/core-wg/href/issues/48
            path = './' + path
        # FIXME: "if the authority component is present and the path component
        # does not match..." (but how can that be in the first place?) and the next two
        uri += path

        if self.query:
            # Conveniently we don't have to strip a leading & because
            # to_uri_part is using join and not prefix-with semantics
            uri += '?' + self.query.to_uri_part()

        if self.fragment is not None:
            uri += "#" + self.fragment.to_uri_part()

        return uri

    known_schemes = {
            -1: 'coap',
            -2: 'coaps',
            -3: 'http',
            -4: 'https',
            -5: 'urn',
            -6: 'did',
            }

if __name__ == "__main__":
    import json
    for testfile in ('tests.json', ):
        testdata = json.load(open(testfile))
        base_uri = testdata['base-uri']
        def h2c(hexdata):
            return CRIRef.from_serialized_unsafe(cbor2.loads(bytes.fromhex(hexdata)))
        base_cri = h2c(testdata['base-cri'])

        assert base_cri.to_uri_ref() == base_uri

        for (i, test) in enumerate(testdata['test-vectors']):
            print(f"Testing {testfile} case {i} ({test.get('description', 'no description')})")
            try:
                cri = h2c(test['cri'])
                resolved_cri = h2c(test['resolved-cri'])
                uri = test['uri']
                resolved_uri = test['resolved-uri']
                uri_from_cri = test['uri-from-cri']

                print(f"  raw={cbor2.loads(bytes.fromhex(test['cri']))}")
                print(f"  {cri=}")
                print(f"  {uri_from_cri=}")
                if uri_from_cri is not None:
                    assert cri.to_uri_ref() == uri_from_cri, "CRI wasn't converted to a URI correctly (produced %s)" % cri.to_uri_ref()
                else:
                    try:
                        cri.to_uri_ref()
                    except NotExpressibleAsUri:
                        pass
                    else:
                        raise AssertionError("to_uri_ref produced URI (%s) even though it should be unexpressible" % cri.to_uri_ref())

                print(f"  {resolved_cri=}")
                if test['resolved-cri'] != '80':
                    assert cri.resolve(base_cri) == resolved_cri, "CRI wasn't resolved correctly (produced %s)" % cri.resolve(base_cri)

                print(f"  {resolved_uri=}")
                assert resolved_cri.to_uri_ref() == resolved_uri, "Resolved CRI wasn't converted to a URI correctly (produced %s)" % resolved_cri.to_uri_ref()

            except Exception as e:
                if 'invalid' in test:
                    print("Raised %s which is OK because the test is invalid (%s)" % (e, test['invalid']))
                else:
                    raise
            else:
                assert 'invalid' not in test
