#!/usr/bin/env python3

from setuptools import setup, find_packages

name = "micrurus"
version = "0.0post0"

setup(
    name=name,
    version=version,
    packages=find_packages(),
    author="Christian Amsüss",
    author_email="chrysn@fsfe.org",
    url="https://gitlab.com/chrysn/aiocoap",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        ],

    # Those could be split up into extras, but it's too early for that yet
    install_requires=[
        'LinkHeader',
        'rdflib',
        'pyyaml',
        'cbor',
        ],
    )
