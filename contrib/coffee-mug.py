#!/usr/bin/env python3
"""A testing implemnetation of the CoRAL coffee machine client

In the coffee machine example setup, this would be the software running on a
coffee mug. It accepts, on the command line, an entry URI which a real mug
might "feel" from an NFC tag inside the heating panel.

It can only act on CoAP URIs and uses the aiocoap library to act on them.

When interacting with a coffee machine, it will follow a set of hard-coded
rules w/rt ingredients, but generally try to brew a single cpu of coffee.

This is currently implemented in a very brutal cross-the-stack fashion, where
CoAP library actions are mixed with managing the browsing context and running
the actual application. It is hoped that from this, insights on how those tasks
are best split up can be derived.
"""

import argparse
from pathlib import Path
import json
import urllib.parse

from aiocoap.util.cli import AsyncCLIDaemon
from aiocoap import *

from micrurus.iri import IRI
from micrurus.body import Link, Form
from micrurus.yoral import load_yoral
from micrurus.coral import load_coral
from micrurus.dictionary import DEFAULT_DICTIONARY, DictionaryPool, null_dictionary

pool = DictionaryPool(Path(__file__).parent.parent.joinpath('dictionaries'))

# FIXME: Never serialize and deserialize CIRIs

def coral_from_response(response):
    ct = response.opt.content_format
    ciri = IRI.decompose(response.get_request_uri())
    if ct == 65535:
        return load_yoral(response.payload.decode('utf8'), ciri, pool)
    elif ct == 65087:
        import cbor
        return load_coral(cbor.loads(response.payload), ciri, pool.get_dictionary(DEFAULT_DICTIONARY))
    else:
        raise Exception("Response not in a known content format")

class MugError(Exception):
    """Errors raised from situations where the mug can't get forward on its own"""

class MugRunner(AsyncCLIDaemon):
    async def start(self):
        p = argparse.ArgumentParser(description=__doc__)
        p.add_argument("startingpoint", help="Start URI for finding a coffee machine")
        opts = p.parse_args()

        self.ctx = await Context.create_client_context()

        await self.start_with_options(**vars(opts))

        # If an exception flies out of this, the CLIDaemon will throw it on and
        # not run_forever.
        self.stop(0)

    async def start_with_options(self, startingpoint):
        response = await self.ctx.request(Message(code=GET, uri=startingpoint)).response_raising

        coral = coral_from_response(response)\
            .convert_dictionary(null_dictionary)\
            .absolutize()

        # FIXME: Find ambient temperature to help guide the later coffee decision

        for l in coral:
            if isinstance(l, Link) and l.relation == "http://coreapps.org/htcpc#menu":
                menu = await self.ctx.request(Message(code=GET, uri=l.target.recompose())).response_raising
                break
        else:
            raise MugError("No menu found")

        menu = json.loads(menu.payload.decode('utf8'))

        # yay business logic
        order = {}
        preferences = {'syrup': ['chocolate', 'vanilla'], 'milk': ['half-and-half', 'skim']}
        for attribute, sequence in preferences.items():
            available = menu.get(attribute, ())
            for nextbest in sequence:
                if nextbest in available:
                    order[attribute] = nextbest
                    break

        for l in coral:
            if isinstance(l, Form) and l.relation == "http://coreapps.org/htcpc#brew":
                accept_ok = method_ok = False
                for field in l.body or ():
                    if field.relation == "http://coreapps.org/coap#accept" and field.target == 65502:
                        accept_ok = True
                        break

                for field in l.body:
                    if field.relation == "http://coreapps.org/coap#method" and field.target == 2:
                        method_ok = True
                        break
                if accept_ok and method_ok:
                    break
                else:
                    pass # It's a form but I can't use it
        else:
            raise MugError("No brewing facility found")

        order = await self.ctx.request(Message(
            code=POST,
            uri=l.target.recompose(),
            payload=json.dumps(order).encode('utf8'),
            content_format=65502,
            )).response_raising
        # FIXME: Don't do that that way
        next_path = urllib.parse.urljoin(order.get_request_uri(), "/" + "/".join(order.opt.location_path))

        request = self.ctx.request(Message(
                code=GET,
                uri=next_path,
                observe=0,
                ))

        done = False
        async for notification in request.observation:
            status_links = coral_from_response(notification)\
                .convert_dictionary(null_dictionary)\
                .absolutize()

            for l in status_links:
                if isinstance(l, Link) and l.relation == "http://coreapps.org/htcpc#status":
                    print("Current status is", l.target)
                    if l.target == "closed":
                        print("I'm done here")
                        # preferable: `break 2`
                        done = True
                else:
                    raise MugError("Notification has no status")
            if done:
                break

    async def shutdown(self):
        await self.ctx.shutdown()

if __name__ == "__main__":
    MugRunner.sync_main()
