from collections import namedtuple

from .iri import IRI
from .body import Body, AnnotatedBody

def load_yoral(input, base_uri, pool) -> AnnotatedBody:
    """Load YAML data and produce a CoRAL body.

    input may be a string, an open file, or anyting else
    yaml.load can stomach"""

    import yaml
    class CoralLoader(yaml.SafeLoader):
        pass

    def load_iri(loader, node):
        uri = loader.construct_scalar(node)
        return IRI.decompose(uri)
    CoralLoader.add_constructor("!ciri", load_iri)

    def load_link(loader, node):
        link = loader.construct_sequence(node)
        return [2, *link]
    CoralLoader.add_constructor("!link", load_link)

    def load_form(loader, node):
        form = loader.construct_sequence(node)
        return [3, *form]
    CoralLoader.add_constructor("!form", load_form)

    def load_ascii(loader, node):
        scalar = loader.construct_scalar(node)
        try:
            return scalar.encode("ascii")
        except UnicodeEncodeError:
            raise ConstructionError(None, None,
                    "ascii tag used on non-ASCII string", node.start_mark)
    CoralLoader.add_constructor("!ascii", load_ascii)

    DeferredAnnotation = namedtuple("DeferredAnnotation", ("data", "dict"))

    def load_annotatedbody(loader, suffix, node):
        # Running the from_cbor in here has subtly bad consequences: It'd
        # be run on the outermost element before the nested lists have been
        # parsed, so all inner bodies would still be empty, and only later
        # would the YAML parser populate the (now unused) lists that it had
        # created earlier.
        items = loader.construct_sequence(node)
        dict = pool.get_dictionary(suffix)
        return DeferredAnnotation(data=items, dict=dict)
    CoralLoader.add_multi_constructor("!coral:", load_annotatedbody)

    loaded = yaml.load(input, CoralLoader)
    if isinstance(loaded, DeferredAnnotation):
        return Body.from_cbor(loaded.data)\
                .annotate(loaded.dict, base_uri)
    else:
        # YoRAL without annotations needs to be in the null dictionary
        return Body.from_cbor(loaded)\
                .annotate(pool.get_dictionary('tag:chrysn@fsfe.org,2018-11-02:null'), base_uri)
