from typing import Union, Dict
from pathlib import Path

from .iri import IRI

import yaml

NULL_DICTIONARY = 'tag:chrysn@fsfe.org,2018-11-02:null'
DEFAULT_DICTIONARY = 'tag:chrysn@fsfe.org,2018-11-02:default'

class UnknownCode(ValueError):
    """Raised when data is looked up in a dictionary that is not registered"""

class Dictionary:
    """Dictionary currently maps numbers to CIRIs, where whether they are
    experssed as text IRIs or CIRIS depends on whether they're used in
    relation-type or not."""

    def __init__(self, mappings: Dict[int, IRI], dict_uri: str):
        self._mappings = mappings
        self._mappings_inv = {v: k for (k, v) in mappings.items()}
        # simply doing both while IRI/CIRI ambiguity is here
        for k, v in mappings.items():
            self._mappings_inv[v.recompose()] = k

        self.uri = dict_uri

    def number_to_object(self, number):
        try:
            return self._mappings[number]
        except KeyError:
            raise UnknownCode("Number %r not in dictionary" % number)

    def object_to_number(self, o):
        return self._mappings_inv.get(o, o)

    def __repr__(self):
        return "<%s %r with %d entries>" % (
                type(self).__name__,
                self.uri,
                len(self._mappings)
                )

# This is a single object as well in addition to being available via the pool
# because converters construct as it or need to convert to it, and don't have
# access to a pool. (Especially, having this available early helps loading
# CoRAL-expressed dictionaries).
null_dictionary = Dictionary({}, NULL_DICTIONARY)

class DictionaryPool:
    """Accessor to a directory of stored dictionaries"""
    def __init__(self, base: Path):
        self.dictionaries = {NULL_DICTIONARY: null_dictionary}

        for file in base.iterdir():
            if file.suffix == '.yaml':
                data = yaml.load(file.open(), Loader=yaml.SafeLoader)
                uri = data['dictionary_name']
                entries = {k: IRI.decompose(v) for (k, v) in data['entries'].items()}
                self.dictionaries[uri] = Dictionary(entries, uri)
            elif file.suffix == '.poral':
                # FIXME, and we should manually construct the null profile for
                # obvious reasons
                pass

    def get_dictionary(self, dictionary_uri: str) -> Dictionary:
        return self.dictionaries[dictionary_uri]
