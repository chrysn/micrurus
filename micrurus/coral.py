from .body import Body, AnnotatedBody

def load_coral(input, base_uri, input_dictionary) -> AnnotatedBody:
    """Load CoRAL data and produce an annotated body

    input has to be cbor.load or cbor.loads'd already.
    """
    return Body.from_cbor(input).annotate(input_dictionary, base_uri)

