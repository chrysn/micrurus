"""A utility to interpret, display and convert CoRAL files"""

import argparse
import functools
from pathlib import Path

from .iri import IRI
from .body import Body
from .util import HasYamlRepr
from . import dictionary
from . import rfccode

DEFAULT_OUTPUT = Path('/dev/stdout')

class ReportableError(Exception):
    pass

def build_parser():
    p = argparse.ArgumentParser(description=__doc__)

    p.add_argument("input", help="File system location to load data from", type=Path)
    p.add_argument("output", help="File system location to save data at", nargs="?", type=Path, default=DEFAULT_OUTPUT)

    p.add_argument("--no-yaml-tag-ciri", help="Leave CIRIs in their CBOR form", action="store_false", dest="yaml_tag_ciri")
    p.add_argument("--no-yaml-tag-linkish", help="Leave links and forms in their CBOR form", action="store_false", dest="yaml_tag_linkish")

    p.add_argument("--base", help="Override the Base URI of the input data", metavar="URI")
    p.add_argument("--root-is-anonymous", help="Override the root node (context) to be a blank node rather than the base URI (relevant for conversion to RDF)", action='store_true')
    p.add_argument("--input-format", help="MIME type to load the input data as")
    p.add_argument("--output-format", help="MIME type to store the output data as")

    p.add_argument("--comparison", help="Don't output anything, but try converting the input to several dictionaries and show a comparison", action='store_true')

    return p

pool = dictionary.DictionaryPool(Path(__file__).parent.parent.joinpath('dictionaries'))

def split_type(type):
    """Given a MIME type, split it into the main type information and the
    dictionary, if given."""

    if ';dictionary=' in type:
        return type.split(';dictionary=', 1)
    else:
        return (type, dictionary.DEFAULT_DICTIONARY)

def run(input, output, base, input_format, output_format, comparison, yaml_tag_ciri, yaml_tag_linkish, root_is_anonymous):
    base = IRI.decompose(base or input.resolve().as_uri())

    # Guess input format

    if input_format is None and input.suffix == '.wlnk':
        input_format = 'application/link-format'

    if input_format is None and input.suffix == '.yoral':
        # TBD: Encode format in some tag
        input_format = 'application/coral+yaml'

    if input_format is None and input.suffix == '.ttl':
        input_format = 'text/turtle'

    if input_format is None:
        raise ReportableError("Input format could not be derived")

    input_format, input_dictionary = split_type(input_format)
    if input_dictionary is not None:
        input_dictionary = pool.get_dictionary(input_dictionary)

    # Guess output format

    if output_format is None and output is DEFAULT_OUTPUT:
        output_format = 'application/coral+yaml'

    if output_format is None and output.suffix == '.dot':
        output_format = 'text/vnd.graphviz'

    if output_format is None and output.suffix == '.ttl':
        output_format = 'text/turtle'

    if output_format is None:
        raise ReportableError("Output format could not be derived")

    output_format, output_dictionary = split_type(output_format)

    # Load input

    if input_format == 'application/link-format':
        import link_header
        from .conversions.linkformat import from_linkformat
        # strip: Be nice to people who create their 6690 files in editors that
        # default to terminating new lines
        links = link_header.parse(input.open().read().strip())
        converted = from_linkformat(links, base, synthetic_rels=True)
    elif input_format == 'application/coral+yaml':
        from .yoral import load_yoral
        with input.open('rt') as infile:
            converted = load_yoral(infile, base, pool)
    elif input_format == 'application/coral+cbor':
        from .coral import load_coral
        import cbor
        with input.open('rb') as infile:
            converted = load_coral(cbor.load(infile), base, input_dictionary)
    elif input_format == 'text/turtle':
        from rdflib import Graph
        from .conversions.rdf import from_rdf
        g = Graph()
        g.parse(input.resolve().as_uri(), format="turtle")
        converted = from_rdf(g).as_annotated_body(base, synthetic_rels=True)
        if root_is_anonymous:
            print("Warning: Converting from RDF with a blank root node doesn't really work because the root node can not be identified", file=sys.stderr)
    else:
        raise ReportableError("Not a supported input format")


    if comparison:
        if output is not DEFAULT_OUTPUT:
            raise ReportableError("The --comparison option is incompatible with a configured output.")

        dictionaries = [
                dictionary.DEFAULT_DICTIONARY,
                'tag:chrysn@fsfe.org,2018-11-02:linkformat',
                dictionary.NULL_DICTIONARY,
                'tag:chrysn@fsfe.org,2018-11-08:rdf',
                ]

        import cbor

        print("Input data: %s bytes" % input.stat().st_size)
        for rname in dictionaries:
            r = pool.get_dictionary(rname)
            size = len(cbor.dumps(converted.convert_dictionary(r).to_cbor()))
            print("%s: %s bytes" % (rname, size))
        return

    if output_dictionary is not None:
        converted = converted.convert_dictionary(pool.get_dictionary(output_dictionary))

    # Export

    if output_format == 'application/coral+cbor':
        import cbor
        with output.open('wb') as of:
            cbor.dump(converted.to_cbor(), of)
    elif output_format == 'application/coral+yaml':
        from yaml import dump

        CoralDumper = HasYamlRepr.build_dumper_class()
        CoralDumper.tag_ciri = yaml_tag_ciri
        CoralDumper.tag_linkish = yaml_tag_linkish

        def enhanced_repr_bytes(representer, item):
            if all(x < 127 for x in item):
                return representer.represent_scalar("!ascii", item.decode("ascii"))
            else:
                return representer.represent_binary(item)
        CoralDumper.add_representer(bytes, enhanced_repr_bytes)

        with output.open('wt') as of:
            dump(converted, of, Dumper=CoralDumper)
    elif output_format == 'text/turtle':
        from .conversions.rdf import to_rdf
        g = to_rdf(converted, root_is_anonymous=root_is_anonymous)

        with output.open('wb') as of:
            of.write(g.serialize(format='turtle'))
    elif output_format == 'text/vnd.graphviz':
        from .conversions.dot import to_dot
        with output.open('w') as of:
            of.write(to_dot(converted))
    else:
        raise ReportableError("Not a supported output format")

def main(args=None):
    p = build_parser()
    opts = p.parse_args(args)

    try:
        run(**vars(opts))
    except ReportableError as e:
        raise p.error(e)

if __name__ == "__main__":
    main()
