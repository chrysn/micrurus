from collections import namedtuple
import warnings

from . import rfccode
from .rfccode import Option, PathType
from .util import HasYamlRepr

class IRI(tuple, HasYamlRepr):
    @classmethod
    def from_cbor(cls, items):
        # This works only as long as all IRI parts have exactly two items
        return cls(zip(items[::2], items[1::2]))

    def to_cbor(self):
        return sum([[
            int(t),
            int(v) if isinstance(v, rfccode.PathType) \
                    else v
            ] for (t, v) in self], [])

    def repr_yaml(self, representer):
        if representer.tag_ciri:
            return representer.represent_scalar('!ciri', self.recompose() if self.is_absolute() else self.recompose_relative())
        else:
            return representer.represent_list(self.to_cbor())

    def is_well_formed(self):
        # This already assumes that self follows the CDDL
        return rfccode.is_well_formed(self)

    def is_absolute(self):
        return rfccode.is_absolute(self)

    def resolve(self, base, relation=None):
        result = rfccode.resolve(base, self, relation)
        if result is None:
            raise ValueError("Resolution failed")
        return IRI(result)

    def __truediv__(self, other):
        return other.resolve(self)

    def recompose(self):
        result = rfccode.recompose(self)
        if result is None:
            raise ValueError("Recomposition failed")
        return result

    def recompose_relative(self):
        """Return the relative reference of this URI, if it is relative and can
        be expressed as a relative reference."""
        from .rfccode import (_encode_reg_name, _encode_ip_address,
                _encode_path_segment, _encode_query_argument, _encode_fragment)
        # FIXME this is rfccode.recompose without the is_absolute check and a
        # PATH_TYPE branch as well as the had_netloc part; that most
        # certainly can be unified

        result = ""
        no_path = True
        had_netloc = False
        first_query = True
        may_strip_dotslash = False
        for (option, value) in self:
          if option == Option.PATH_TYPE:
            had_netloc = True
            if value == PathType.ABSOLUTE_PATH:
              pass
            elif value == PathType.RELATIVE_PATH:
              # That's a cheap (in terms of effort, not efficiency) way to
              # avoid path parts being mistaken for a scheme.
              # Also, not fully checked if that has the precise semantics
              result = "./"
              may_strip_dotslash = True
              no_path = False
            elif value == PathType.APPEND_RELATION or \
                value == PathType.APPEND_PATH:
              raise ValueError("Can't express append types in a URI refrence")
            else:
              steps_up = value - PathType.RELATIVE_PATH_1UP + 1
              result = "../" * steps_up
              result = result[:-1] # no trailing slash
              no_path = False
              had_netloc = True
          if option == Option.SCHEME:
            result += value + ":"
          elif option == Option.HOST_NAME:
            result += "//" + _encode_reg_name(value)
            had_netloc = True
          elif option == Option.HOST_IP:
            result += "//" + _encode_ip_address(value)
          elif option == Option.PORT:
            result += ":" + str(value)
          elif option == Option.PATH:
            if not had_netloc:
              assert result == ""
              # See RELATIVE_PATH branch above: This is cheap
              result = "."
              had_netloc = True
              may_strip_dotslash = True
            result += "/" + _encode_path_segment(value)
            if may_strip_dotslash:
              if value and ':' not in value:
                assert result.startswith('./')
                result = result[2:]
              may_strip_dotslash = False
            no_path = False
          elif option == Option.QUERY:
            if no_path and had_netloc:
              result += "/"
              no_path = False
            result += "?" if first_query else "&"
            result += _encode_query_argument(value)
            first_query = False
          elif option == Option.FRAGMENT:
            if no_path and had_netloc:
              result += "/"
              no_path = False
            result += "#" + _encode_fragment(value)
        if no_path and had_netloc:
          result += "/"
          no_path = False
        return result

    def __repr__(self):
        if self.is_well_formed():
            if self.is_absolute():
                prefix = ""
                recomposed = self.recompose()
            else:
                prefix = "reference "
                recomposed = self.recompose_relative()
            return '<IRI %s%r>' % (prefix, recomposed)
        else:
            return '<IRI malformed %s>' % (tuple.__repr__(self),)

    @classmethod
    def decompose(cls, uri):
        # FIXME:
        # * This might not do all of the required escaping properly
        # * This produces port-less option lists if the option is absent.
        # * This assume that empty queries and fragments are also absent
        # * This may not distinguish correctly between x://y/ and x://y, if
        #   that is a distinction for the x scheme (I only know for sure they
        #   are equivalent for CoAP)
        # * This produces an empty-string hostname option for :/// URIs; not
        #   sure whether that's aligned with the current draft's intention.

        from urllib.parse import urlparse, unquote
        import ipaddress

        # FIXME: workaround for urlparse, which wrongly assumes there'd be any
        # value in splitting off params; explicitly percent-escaping them to
        # avoid triggering urllib's param splitting; they'll be unescaped later
        # anyway
        parsed = urlparse(uri.replace(';', '%3b'))

        options = []

        if parsed.scheme:
            options.append((Option.SCHEME, parsed.scheme))

        if parsed.hostname is not None:
            if parsed.netloc.startswith('[') and '[' not in parsed.hostname:
                # It's an IPv6 address
                addr = ipaddress.IPv6Address(parsed.hostname)
                options.append((Option.HOST_IP, addr.packed))
            # FIXME: What if it looks like it but is not?
            elif all(c in '0123456789.' for c in parsed.hostname):
                addr = ipaddress.IPv4Address(parsed.hostname)
                options.append((Option.HOST_IP, addr.packed))
            else:
                options.append((Option.HOST_NAME, unquote(parsed.hostname)))
        if parsed.hostname is None and ':///' in uri:
            # FIXME the criterion is not 100% precise as it'd catch
            # foo:bar:///x as well
            options.append((Option.HOST_NAME, ""))
        if parsed.port is not None:
            options.append((Option.PORT, parsed.port))

        if not parsed.scheme and parsed.hostname is None and \
                parsed.port is None and parsed.path.startswith('/'):
            options.append((Option.PATH_TYPE, PathType.ABSOLUTE_PATH))

        if parsed.path in ('', '/'):
            path = []
        else:
            if parsed.hostname:
                assert parsed.path.startswith('/'), "Odd path: %s" % (parsed,)
            path = parsed.path.lstrip('/').split('/')
        #: Number of relative levels up, if applicable
        upcount = 0
        for p in path:
            if p == '.':
                pass
            elif p == '..':
                if options and options[-1][0] == Option.PATH:
                    options.pop(-1)
                else:
                    # either there's a path-absolute start (like (PATH_TYPE
                    # ABSOLUTE) or a netloc), in which those will just go to void, or the'll be summed in the end
                    upcount += 1
            else:
                options.append((Option.PATH, unquote(p)))
        if (not options or options[0][0] == Option.PATH) and upcount != 0:
            options.insert(0, (
                Option.PATH_TYPE,
                PathType.RELATIVE_PATH_1UP - 1 + upcount
                ))

        if parsed.query:
            for q in parsed.query.split('&'):
                options.append((Option.QUERY, unquote(q)))

        if parsed.fragment:
            options.append((Option.FRAGMENT, unquote(parsed.fragment)))

        self = cls(options)

        recomposed = self.recompose() if self.is_absolute() else self.recompose_relative()
        if recomposed != uri:
            warnings.warn("Recomposition failed: Expected %r, got %r (options: %r, parsed: %s)" % (uri, recomposed, options, parsed), stacklevel=2)

        return self

    def unresolve(self, base, relation=None):
        if not self.is_absolute() or not base.is_absolute():
            raise ValueError("unresolve only works on absolute URIs")
        self_origin = [v for v in self if v[0] < Option.PATH_TYPE]
        base_origin = [v for v in base if v[0] < Option.PATH_TYPE]
        if self_origin == base_origin:
            return type(self)([(Option.PATH_TYPE, PathType.ABSOLUTE_PATH), *(v for v in self if v[0] > Option.PATH_TYPE)])
        # that can always be done cheaply
        return self
