from typing import Optional
from dataclasses import dataclass

from .util import HasYamlRepr
from .iri import IRI
from .dictionary import Dictionary
from collections import namedtuple

class BaseDirective(HasYamlRepr):
    def __init__(self, iri):
        self.iri = iri

    @classmethod
    def from_cbor(cls, data):
        assert len(data) == 2
        return cls(IRI.from_cbor(data[1]))

    def to_cbor(self):
        return [1, self.iri.to_cbor()]

    def repr_yaml(self, representer):
        return representer.represent_list(self.to_yaml())

    def to_yaml(self):
        return [1, self.iri]

    def convert_dictionary(self, input_dictionary, output_dictionary):
        return self

class _LinkIsh:
    @classmethod
    def from_cbor(cls, items):
        relation = items[1]
        if isinstance(items[2], list):
            target = IRI.from_cbor(items[2])
        else:
            target = items[2]
        body = Body.from_cbor(items[3]) if len(items) == 4 else None
        return cls(relation, target, body)

    def to_cbor(self):
        target_cbor = self.target.to_cbor() if isinstance(self.target, IRI) else self.target
        if not self.body: # either None or empty list
            return list((self._linkish_code, self.relation, target_cbor))
        else:
            return list((self._linkish_code, self.relation, target_cbor, self.body.to_cbor()))

    def to_yaml(self):
        if not self.body: # either None or empty list
            return list((self.relation, self.target))
        else:
            return list((self.relation, self.target, self.body.to_yaml()))

    def repr_yaml(self, representer):
        if representer.tag_linkish:
            return representer.represent_sequence(self._linkish_tag, self.to_yaml())
        else:
            return representer.represent_list([self._linkish_code] + self.to_yaml())

    def convert_dictionary(self, input_dictionary, output_dictionary):
        if isinstance(self.relation, int):
            rel = input_dictionary.number_to_object(self.relation)
        else:
            rel = self.relation
        rel = output_dictionary.object_to_number(rel)
        return type(self)(rel, self.target, self.body.convert_dictionary(input_dictionary, output_dictionary) if self.body else None)

    def absolutize(self, base):
        if isinstance(self.target, IRI):
            target = self.target.resolve(base, self.relation)
        else:
            # None or literals, but none of those change the base
            target = self.target
        return type(self)(
                self.relation,
                target,
                None if self.body is None else self.body.absolutize(target or base),
                )

class Link(_LinkIsh, HasYamlRepr, namedtuple("_Link", ("relation", "target", "body"))):
    _linkish_code = 2
    _linkish_tag = '!link'

class Representation(namedtuple("_Representation", ("data", "body")), HasYamlRepr):
    @classmethod
    def from_cbor(cls, items):
        assert len(items) in (2, 3)
        assert items[0] == 0
        assert isinstance(items[1], bytes)
        body = Body.from_cbor(items[2]) if len(items) == 3 else None
        return cls(items[1], body)

    def to_cbor(self):
        if self.body is None:
            return [0, self.data]
        else:
            return [0, self.data, self.body.to_yaml()]

    def repr_yaml(self, representer):
        return representer.represent_list(self.to_cbor())

    def convert_dictionary(self, input_dictionary, output_dictionary):
        return self

class Form(_LinkIsh, HasYamlRepr, namedtuple("_Form", ("relation", "target", "body"))):
    _linkish_code = 3
    _linkish_tag = '!form'

class Body(list):
    @classmethod
    def from_cbor(cls, items):
        data = []
        for i in items:
            if i[0] == Link._linkish_code:
                i = Link.from_cbor(i)
            elif i[0] == Form._linkish_code:
                i = Form.from_cbor(i)
            elif i[0] == 1:
                i = BaseDirective.from_cbor(i)
            elif i[0] == 0:
                i = Representation.from_cbor(i)
            else:
                raise AssertionError("Incoming data was assumed to be validated against CCDL but isn't: Unknown key %s" % i[0])
            data.append(i)
        return cls(data)

    def to_cbor(self):
        return [x.to_cbor() for x in self]

    def to_yaml(self):
        return list(self)

    def convert_dictionary(self, input_dictionary, output_dictionary):
        return type(self)(l.convert_dictionary(input_dictionary, output_dictionary) for l in self)

    def annotate(self, dictionary: Dictionary, base: Optional[IRI]) -> "AnnotatedBody":
        """Return a shallow copy of self as an AnnotatedBody with the given
        dictionary and base value"""

        new = AnnotatedBody(self)
        new.dictionary = dictionary
        new.base = base
        return new

    def absolutize(self, base):
        ret = Body()

        for i in self:
            if isinstance(i, Link) or isinstance(i, Form):
                ret.append(i.absolutize(base))
            elif isinstance(i, Representation):
                if i.body is not None:
                    i.body.absolutize_in_place(base)
                ret.append(i)
            elif isinstance(i, BaseDirective):
                base = base / i.iri
            else:
                raise NotImplementedError("Absolutization is not implemented for %s" % type(i))
        return ret

class AnnotatedBody(Body, HasYamlRepr):
    """A Body that also carries information about its base URI and profile.

    To create, load or build a body and run .annotate() on it."""

    def convert_dictionary(self, output_dictionary):
        return Body.convert_dictionary(self, self.dictionary, output_dictionary)\
                .annotate(output_dictionary, self.base)

    def repr_yaml(self, representer):
        dictname = self.dictionary.uri
        return representer.represent_sequence('!coral:' + dictname, self.to_yaml())

    def absolutize(self):
        """Change all links to absolute CIRIs, and remove all base statements"""

        return Body.absolutize(self, self.base).annotate(self.dictionary, self.base)
