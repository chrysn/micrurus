from pydotplus import Graph, Node, Edge
import sys

from ..body import Link, Representation, BaseDirective, Form
from ..iri import IRI

def _paint_body(body, rootnode, graph):
    """Paint all children of a body and the node itself to a pydot graph, and
    return the root node"""
    for item in body:
        if isinstance(item, Link):
            if isinstance(item.target, IRI):
                linknode = Node(graph.get_next_sequence_number(), label=str(item.target))
            else:
                linknode = Node(graph.get_next_sequence_number(), label=repr(item.target), shape="box")
            graph.add_node(linknode)
            graph.add_edge(Edge(rootnode, linknode, label=str(item.relation)))
            if item.body:
                _paint_body(item.body, linknode, graph)
        elif isinstance(item, Representation):
            text = repr(item.data)
            if len(text) > 20:
                text = text[:10] + "…" + text[-5:]
            reprnode = Node(graph.get_next_sequence_number(), label=text, shape="box3d")
            graph.add_node(reprnode)
            graph.add_edge(Edge(rootnode, reprnode))
            if item.body:
                _paint_body(item.body, reprnode, graph)
        elif isinstance(item, BaseDirective):
            baseset = Node(graph.get_next_sequence_number(), label="with base %r" % (item.iri,))
            graph.add_node(baseset)
            graph.add_edge(Edge(rootnode, baseset))
            rootnode = baseset
        elif isinstance(item, Form):
            formnode = Node(graph.get_next_sequence_number(), label="a form to\n" + str(item.target), shape="octagon")
            graph.add_node(formnode)
            graph.add_edge(Edge(rootnode, formnode, label=str(item.relation)))
            if item.body:
                _paint_body(item.body, formnode, graph)
        else:
            print("Not painted: %s" % (item,), file=sys.stderr)

def to_dot(body):
    g = Graph()
    rootnode = Node("root", label="this", shape="diamond")
    g.add_node(rootnode)
    root = _paint_body(body, rootnode, g)
    return g.to_string()
