# This assumes that all input data is in Modernzied Link Format as described in
# draft-ietf-core-resource-directory-14..17

from collections import defaultdict

from ..body import Body, Link, AnnotatedBody
from ..iri import IRI
from ..dictionary import null_dictionary

import link_header

SYNTHETIC_REL = 'tag:chrysn@fsfe.org,2018-11-04:wants-to-tell-you-something-about'

class BlankNode:
    def unresolve(self, *args):
        return None

class LiteralNode:
    # All literals are distinct, but can have properties -- at least that's the
    # currently in-development model to support language-tagged strings
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return "<%s at %#x value %r>" % (type(self).__name__, id(self), self.value)

    def unresolve(self, *args):
        return self.value

# This is what since https://github.com/core-wg/coral/pull/1 is called "basic information model"
class LinkCollection(defaultdict):
    """Alternative object that represents the same link information as an
    AnnotatedBody (except that it does not have a dictionary and base URI), but
    is an easily mutable flat structure of arbitrary links"""

    # keys: IRI-ish objects
    # values: lists of statements about that IRI, where statements are in (rel:
    #     IRI, target: IRI-ish) form, and an IRIish is something that has a
    #     unresolve() method that returns what'll go into a link's target
    #     field (eg. None for blank URIs, or literal values). They need to be
    #     hashable as they can be keys again.

    def __init__(self):
        super().__init__(lambda: [])

    def as_annotated_body(self, uri_base, *, synthetic_rels=False):
        """Greedily build tree of it in the null profile. Consumes the items in
        self."""

        stack = [(uri_base, uri_base, Body([]))]

        while self:
            node, uri, links = stack[-1]

            while self.get(node):
                rel, target = self[node].pop(0)
                new_links = Body([])

                # For IRI nodes, this gives a usable value for the target field
                # too. (FIXME: below we'll have to distinguish anyway to see
                # whether there's a new base uri to inherit or not; could just
                # as well distinguish here and do something cleaner than
                # unresolve.)
                #
                # FIXME is that actualy the rel we should pass in here? or a part of the rel_uri?
                relative_target = target.unresolve(uri, rel)

                links.append(Link(IRI.decompose(rel), relative_target, new_links))
                stack.append((target, target if isinstance(target, IRI) else uri, new_links))
                break
            else:
                if len(stack) == 1:
                    if synthetic_rels:
                        if any(self.values()):
                            # Try finding *a* root -- we could also just pick the
                            # "first in the dict" (risking many synthetic rels), or
                            # think more about whether it might make a difference
                            # which root we find.
                            current = (k for (k, v) in self.items() if v).__next__()
                            # Limit iterations in case there are loops in the
                            # unreachable links
                            for i in range(10):
                                for k, values in self.items():
                                    for r, v in values:
                                        if v == current:
                                            current = k
                                else:
                                    break
                            self[uri_base].append((SYNTHETIC_REL, current))
                            continue
                    return stack[0][2].annotate(null_dictionary, uri_base)
                else:
                    stack.pop(-1)
        raise RuntimeError

def from_linkformat(links: link_header.LinkHeader, uri_base: IRI, *, synthetic_rels=False) -> AnnotatedBody:
    rels = LinkCollection()

    uri_base_str = uri_base.recompose()

    for l in links.links:
        target = IRI.decompose(l.get_target(uri_base_str))
        anchor = IRI.decompose(l.get_context(uri_base_str))
        had_rel = False
        for k, v in l.attr_pairs:
            if k == 'rel':
                had_rel = True
                for r in v.split(' '):
                    if ':' not in r:
                        r = 'http://www.iana.org/assignments/relation/' + r
                    rels[anchor].append((r, target))
            elif k == 'rev':
                had_rel = True
                for r in v.split(' '):
                    if ':' not in r:
                        r = 'http://www.iana.org/assignments/relation/' + r
                    rels[target].append((r, anchor))
            elif k == 'anchor':
                pass # already processed
            else:
                # Compensating for a bad design decision in RFC6690
                if k in ('ct', 'if', 'rt'):
                    for v in v.split():
                        # it might make sense to make value into a URI here
                        attr_uri = 'http://tbdx/separate/' + k
                        rels[target].append((attr_uri, LiteralNode(v)))
                else:
                    if ':' in k:
                        attr_uri = k
                    else:
                        attr_uri = 'http://tbd2/' + k
                    rels[target].append((attr_uri, LiteralNode(v)))
        if not had_rel:
            rels[anchor].append(('http://www.iana.org/assignments/relation/hosts', target))

    return rels.as_annotated_body(uri_base, synthetic_rels=synthetic_rels)
