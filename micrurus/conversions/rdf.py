from rdflib import URIRef, Literal, BNode, Namespace
from rdflib.term import Identifier

from ..body import Link, AnnotatedBody, BaseDirective, Form
from ..iri import IRI
from ..dictionary import null_dictionary

from .linkformat import LinkCollection, BlankNode, LiteralNode

coral = Namespace('tag:tbd:coral,')
rdf = Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')

def collect_statements(body, context: Identifier):
    """Iterate over all RDF statements in body. Requires body to conform to null dictionary and be absolutized."""

    for i in body:
        if isinstance(i, Link):
            if isinstance(i.target, IRI):
                rdf_target = URIRef(i.target.recompose())
                if i.body is not None:
                    yield from collect_statements(i.body, rdf_target)
            else:
                rdf_target = Literal(i.target)
                if i.body is not None:
                    # Can't produce statements about this, so pick out what we
                    # can represent or fail
                    for stmt in i.body:
                        # namespacelang sounds odd, but that's what you get
                        # when you do a naïve take on the RDF XML format
                        if isinstance(stmt, Link) and \
                                stmt.relation.recompose() == 'http://www.w3.org/XML/1998/namespacelang' and \
                                isinstance(stmt.target, str) and \
                                stmt.body is None:
                            rdf_target = Literal(rdf_target, lang=stmt.target)
                        else:
                            raise ValueError("Can't encode this statement about the literal: %s" % (stmt,))
            assert isinstance(i.relation, IRI) and i.relation.is_absolute()
            rel = URIRef(i.relation.recompose())
            yield (context, rel, rdf_target)
        elif isinstance(i, BaseDirective):
            raise AssertionError("Base directive present in to-be absolutized input")
        elif isinstance(i, Form):
            # You might think it'd make sense to first run a denormalization
            # step to create a "regular" CoRAL link to a blank node out of the
            # form, but that'd at least not be completely trivial as all
            # non-literal targets would need their CIRIs rewritten.
            if not isinstance(i.target, IRI):
                raise ValueError("Form does not point to a target IRI")

            the_form = BNode()

            rdf_target = URIRef(i.target.recompose())
            if i.body is not None:
                yield from collect_statements(i.body, the_form, i.target)

            assert ':' in i.relation, "Relation must be a URI"
            rel = URIRef(i.relation)
            yield (the_form, rdf.type, coral.form)
            yield (the_form, coral.submission_uri, rdf_target)

            yield (context, rel, the_form)
        else:
            raise ValueError("Can't encode this part into RDF: %s" % (i,))

def to_rdf(body: AnnotatedBody, root_is_anonymous=False):
    import rdflib
    g = rdflib.Graph()

    for s in collect_statements(body.convert_dictionary(null_dictionary).absolutize(), rdflib.BNode() if root_is_anonymous else rdflib.URIRef(body.base.recompose())):
        g.add(s)

    return g

def from_rdf(statements) -> LinkCollection:
    collection = LinkCollection()

    # might use them directly as well if .unresolve() moves from a method call
    # to a function where we could register that unresolving is always None for
    # them
    blank_nodes = {}

    for s, p, o in statements:
        if isinstance(s, URIRef):
            s = IRI.decompose(s.toPython())
        elif isinstance(s, BNode):
            s = blank_nodes.setdefault(s, BlankNode())
        else:
            raise ValueError("Subject not expressible")

        if isinstance(p, URIRef):
            p = p.toPython()
        else:
            raise ValueError("Predicate not expressible")

        if isinstance(o, URIRef):
            o = IRI.decompose(o.toPython())
        elif isinstance(o, BNode):
            o = blank_nodes.setdefault(o, BlankNode())
        elif isinstance(o, Literal):
            # FIXME: Could probably do some xsd:int to CBOR integer conversion
            lang = o.language
            o = LiteralNode(o.toPython())
            if lang is not None:
                collection[o].append(('http://www.w3.org/XML/1998/namespacelang', LiteralNode(lang)))
            # FIXME: data type information thown away
        else:
            raise ValueError("Object not expressible")

        collection[s].append((p, o))

    return collection
