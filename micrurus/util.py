class HasYamlRepr:
    """Base class that allows any object to implement its onw YAML
    representation without having to be explicitly hooked with add_representer.

    As add_representer works per class and not per subclass, this clas keeps
    track of its subclasses; run .build_dumper_class() onHasYamlRepr to get a
    dumper class that has all HasYamlRepr classes added with their
    representers.
    """
    def repr_yaml(self, representer):
        raise NotImplementedError("Children of HasYamlRepr must implement repr_yaml")

    _subclasses = []

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        cls._subclasses.append(cls)

    @classmethod
    def build_dumper_class(cls):
        from yaml import Dumper

        class CoralDumper(Dumper):
            pass

        for s in cls._subclasses:
            CoralDumper.add_representer(s, lambda representer, item: item.repr_yaml(representer))

        return CoralDumper
