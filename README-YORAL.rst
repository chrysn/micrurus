The YoRAL format
================

YoRAL is a variation of CoRAL used by micrurus.
Its goal is to be both usable as a debug view of binary CoRAL
both for input and output
(it's easier than editing CBOR while keeping all of CoRAL's CBOR idioms and compressions)
and as a more convenient editable format that uses tags
(eg. rather than writing a CIRI, "!ciri '/path'" can be written).

Tags can be intermixed arbitrarily with CBOR-style YoRAL.

Supported tags:

* !coral:$DICT: Usable around the top-level element,
  serves both as a magic number and to encode the URI of the used dictionary.

* !link [r, t, b?]: Shorthand for [2, r, t, b?]
* !form [r, t, b?]: Shorthand for [3, r, t, b?]
* !ciri '$URI': Expands to a CIRI equivalent to the $URI reference
* !ascii '$STRING': Expands to the string encoded in ASCII
  (convenient to create a bytes object that happens to contain ASCII characters,
  like Python's b"$STRING")

Future
------

With the addition of EDN_ to CoRAL,
the pressure to have YoRAL has decreased,
and it may eventually fall out of use.
This will not happen until there is a convenient way to process and produce CBOR diagnostic notation in Python.

.. _EDN: https://datatracker.ietf.org/doc/draft-bormann-cbor-edn-literals/
