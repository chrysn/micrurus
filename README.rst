micrurus
========

micrurus is an implementation of CoRAL_ in Python_. The name is because
micrurus_ is a genus of coral snake.

.. _CoRAL: https://datatracker.ietf.org/doc/draft-ietf-core-coral/
.. _Python: https://www.python.org/
.. _micrurus: https://en.wikipedia.org/wiki/Micrurus

Draft versions
--------------

The currently implemented CoRAL version is somewhere around -04 (unpublished).

Notable differences in current version:

* CIRIs are still in use, based on an older version of -href.
* Form support is terrible.
* The dictionary is not going through Packed CBOR yet.

Example usage
-------------

The provided ``micrurus-util`` program is primarily useful
to convert variations of CoRAL,
and other data formats that have an equivalent CoRAL representation.

One thing micrurus can do right now is to convert link format to CoRAL to RDF::

    $ pip3 install LinkHeader
    $ ./micrurus-util 6690example.wlnk --base http://example.com/.well-known/core --output-format 'text/turtle'

This takes the example link-format contents of RFC6690, expresses them in CoRAL
and turns the CoRAL data into Turtle RDF. Alternatively, you can leave off the
output-format option to receive "YoRAL" (CBOR CoRAL but printed in YAML for
readability with some abbreviations) with the default dictionary, or run::

    $ ./micrurus-util 6690example.wlnk --base http://example.com/.well-known/core --output-format 'application/coral+yaml;dictionary=tag:chrysn@fsfe.org,2018-11-02:linkformat'

to use the link format dictionary (shipped with micrurus in
./dictionaries/linkformat.yaml), which compresses the example quite a bit more.

Dictionary comparison
---------------------

micrurus-util can run a number of dictionaries on the same input for comparison::

    $ ./micrurus-util 6690example.wlnk --base http://example.com/.well-known/core --comparison
    Input data: 252 bytes
    tag:chrysn@fsfe.org,2018-11-02:default: 562 bytes
    tag:chrysn@fsfe.org,2018-11-02:linkformat: 185 bytes
    tag:chrysn@fsfe.org,2018-11-02:null: 562 bytes
    tag:chrysn@fsfe.org,2018-11-08:rdf: 562 bytes

CoRAL-CoRAL conversion
----------------------

For a different example, let's convert CoRAL to CoRAL; the shipped
spiderman.joral example file is expressed in the null dictionary (ie. can be read
with any dictionary)::

    $ ./micrurus-util spiderman.yoral --input-format application/coral+yaml --output-format text/turtle

Supported formats
-----------------

micrurus currently supports:

* CoRAL binary format in ``application/coral+cbor`` with dictionary parameter
* "YoRAL" (``application/coral+yaml``),
  a non-standard format that is the trivial conversion of CoRAL binary format to YAML_.
  Various tags are used to make the result more read- and editable.
  For example, ``!ciri "http://example.com"`` expands to an equivalent CIRI.
  The YAML tag ``!coral:...`` around the root element is used to override the dictionary.
* Turtle_ serialization of RDF_ (``text/turtle``).
  For details of the conversion, see the the `RDF PR`_.

The CoRAL diagnostic format is currently not supported.

As input format, the following format is supported:

* `Link format`_ (``application/link-format``). See below for conversion details.

As output format, the following format is supported:

* `graphviz`_ formatted graphs (``text/vnd.graphviz``).
  The tools accept a dictionary parameter to the media type even though that's not technically correct;
  that can be used to start a dictionary conversion in the input.
  Edges and other dictionary items will use dictionary keys as labels,
  resulting in more compact visual representations of compact binary documents.

.. _YAML: https://yaml.org/
.. _Turtle: https://yaml.org/
.. _RDF: https://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/
.. _`RDF PR`: https://github.com/core-wg/coral/pull/2
.. _`Link format`: https://tools.ietf.org/html/rfc6690
.. _`graphviz`: https://graphviz.org/

Dictionaries
------------

Dictionaries are currently loaded from hard-coded mappings between URIs and files
in ``./dictionaries``. Known dictionaries are:

* The default dictionary, assigned the name ``tag:chrysn@fsfe.org,2018-11-02:default``
* The null dictionary (``tag:chrysn@fsfe.org,2018-11-02:null``), which has no
  numeric mappings at all and is used internally -- a data structure using this
  dictionary can access all relation values explicitly.
* An ad-hoc link-format dictionary
  (``tag:chrysn@fsfe.org,2018-11-02:linkformat``), particularly suitable for
  conversions of lists of links, also when used by a Resource Directory.
* An ad-hoc RDF dictionary (``tag:chrysn@fsfe.org,2018-11-08:rdf``),
  particularly suitable for expressing foaf-heavy RDF in CoRAL.

Assorted examples
-----------------

::

    $ ./micrurus-util full-batch.yoral --output-format application/coral+yaml
    $ ./micrurus-util full-batch.yoral full-batch.dot && circo full-batch.dot -Tpng > full-batch.png
    $ ./micrurus-util error-response.yoral --output-format text/turtle --root-is-anonymous
